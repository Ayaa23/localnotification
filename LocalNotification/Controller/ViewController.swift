//
//  ViewController.swift
//  LocalNotification
//
//  Created by aya reda on 10/25/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController , UNUserNotificationCenterDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound , .badge], completionHandler: { didAllow , Error in })
        
    }

   
    @IBAction func GoBtn(_ sender: Any) {
        
     
        let notify = UNMutableNotificationContent()
        notify.title = "60 secs down"
        notify.body = "time is almost up"
        notify.badge = 1
        
        let tirrger = UNTimeIntervalNotificationTrigger(timeInterval: 60 , repeats : true)
        let request = UNNotificationRequest(identifier : "timer" , content : notify , trigger : tirrger)
        UNUserNotificationCenter.current().add( request, withCompletionHandler: nil)
        
    
    }
    

}

