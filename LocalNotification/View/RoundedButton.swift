//
//  RoundedButton.swift
//  LocalNotification
//
//  Created by aya reda on 10/25/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable

class RoundedButton: UIButton {
    
    @IBInspectable var cornerRaduis :CGFloat = 3.0{
        didSet {
            self.layer.cornerRadius = cornerRaduis
        }
       
    }
    
    override func awakeFromNib() {
        self.setUpView()
    }
    
    func setUpView()  {
        self.layer.cornerRadius = cornerRaduis
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setUpView()
    }
    
    
    
    
    
    
}

